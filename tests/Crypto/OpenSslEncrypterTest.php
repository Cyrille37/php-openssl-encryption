<?php

namespace Cyrille37\Tests\Crypto ;

use RuntimeException;
use PHPUnit\Framework\TestCase;
use Cyrille37\Crypto\OpenSslEncrypter ;
use Cyrille37\Crypto\DecryptException ;
use Cyrille37\Crypto\EncryptException ;

class OpenSslEncrypterTest extends TestCase
{
    public function testEncryption()
    {
        $e = new OpenSslEncrypter(str_repeat('a', 16));
        $encrypted = $e->encrypt('foo');
        $this->assertNotEquals('foo', $encrypted);
        $this->assertEquals('foo', $e->decrypt($encrypted));
    }

    public function testRawStringEncryption()
    {
        $e = new OpenSslEncrypter(str_repeat('a', 16));
        $encrypted = $e->encryptString('foo');
        $this->assertNotEquals('foo', $encrypted);
        $this->assertEquals('foo', $e->decryptString($encrypted));
    }

    public function testEncryptionUsingBase64EncodedKey()
    {
        $e = new OpenSslEncrypter(random_bytes(16));
        $encrypted = $e->encrypt('foo');
        $this->assertNotEquals('foo', $encrypted);
        $this->assertEquals('foo', $e->decrypt($encrypted));
    }

    public function testWithCustomCipher()
    {
        $e = new OpenSslEncrypter(str_repeat('b', 32), 'AES-256-CBC');
        $encrypted = $e->encrypt('bar');
        $this->assertNotEquals('bar', $encrypted);
        $this->assertEquals('bar', $e->decrypt($encrypted));

        $e = new OpenSslEncrypter(random_bytes(32), 'AES-256-CBC');
        $encrypted = $e->encrypt('foo');
        $this->assertNotEquals('foo', $encrypted);
        $this->assertEquals('foo', $e->decrypt($encrypted));
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage The only supported ciphers are AES-128-CBC and AES-256-CBC with the correct key lengths.
     */
    public function testDoNoAllowLongerKey()
    {
        new OpenSslEncrypter(str_repeat('z', 32));
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage The only supported ciphers are AES-128-CBC and AES-256-CBC with the correct key lengths.
     */
    public function testWithBadKeyLength()
    {
        new OpenSslEncrypter(str_repeat('a', 5));
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage The only supported ciphers are AES-128-CBC and AES-256-CBC with the correct key lengths.
     */
    public function testWithBadKeyLengthAlternativeCipher()
    {
        new OpenSslEncrypter(str_repeat('a', 16), 'AES-256-CFB8');
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage The only supported ciphers are AES-128-CBC and AES-256-CBC with the correct key lengths.
     */
    public function testWithUnsupportedCipher()
    {
        new OpenSslEncrypter(str_repeat('c', 16), 'AES-256-CFB8');
    }

    /**
     * @expectedException \Cyrille37\Crypto\DecryptException
     * @expectedExceptionMessage The payload is invalid.
     */
    public function testExceptionThrownWhenPayloadIsInvalid()
    {
        $e = new OpenSslEncrypter(str_repeat('a', 16));
        $payload = $e->encrypt('foo');
        $payload = str_shuffle($payload);
        $e->decrypt($payload);
    }

    /**
     * @expectedException \Cyrille37\Crypto\DecryptException
     * @expectedExceptionMessage The MAC is invalid.
     */
    public function testExceptionThrownWithDifferentKey()
    {
        $a = new OpenSslEncrypter(str_repeat('a', 16));
        $b = new OpenSslEncrypter(str_repeat('b', 16));
        $b->decrypt($a->encrypt('baz'));
    }

    /**
     * @expectedException \Cyrille37\Crypto\DecryptException
     * @expectedExceptionMessage The payload is invalid.
     */
    public function testExceptionThrownWhenIvIsTooLong()
    {
        $e = new OpenSslEncrypter(str_repeat('a', 16));
        $payload = $e->encrypt('foo');
        $data = json_decode($payload, true);
        $data['iv'] .= $data['value'][0];
        $data['value'] = substr($data['value'], 1);
        $modified_payload = json_encode($data);
        $e->decrypt($modified_payload);
    }

    /**
     * md5 produce 16 bytes (128 bits) bits key length
     * sha2 produce 32 bytes (256 bits) key length
     */
    public function testKeysLength()
    {
        $k = hash('ripemd256', random_bytes(1024), true);
        $this->assertEquals( 32, mb_strlen($k, '8bit') );

        $k = hash('sha256', random_bytes(1024), true);
        $this->assertEquals( 32, mb_strlen($k, '8bit') );

        $k = hash('md5', random_bytes(1024), true);
        $this->assertEquals( 16, mb_strlen($k, '8bit') );
    }

    /**
     * iv length:
     * AES-128-CBC : 16 bytes (128 bits)
     * AES-256-CBC : 16 bytes (128 bits)
     */
    public function testIvLength()
    {
        $iv_length = openssl_cipher_iv_length('AES-128-CBC');
        $this->assertEquals( 16, $iv_length );

        $iv_length = openssl_cipher_iv_length('AES-256-CBC');
        $this->assertEquals( 16, $iv_length );

    }
}
