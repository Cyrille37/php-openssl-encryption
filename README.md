Fork from Laravel illuminate/encryption whitout requirements.

Install:
```
composer install
```

Run tests:
```
./vendor/bin/phpunit ./tests
```

Run benchmarks:
```
./vendor/bin/phpunit ./benchmarks
```

## Benchmarks results

```
$ ./vendor/bin/phpunit ./benchmarks
PHPUnit 6.5.14 by Sebastian Bergmann and contributors.

Runtime:       PHP 7.3.24-3+ubuntu18.04.1+deb.sury.org+1
Configuration: ./phpunit.xml

.                                                                   1 / 1 (100%)
Intel(R) Core(TM) i7-10510U CPU @ 1.80GHz

ENCRYPT
CYPHER       CLEAR_LEN    CRYPT_LEN    LOOPS        Ellapsed
AES-128-CBC  100          271          100          0.00075602531433105
AES-128-CBC  100          273          1000         0.0071899890899658
AES-256-CBC  100          272          100          0.00072193145751953
AES-256-CBC  100          275          1000         0.0072238445281982
AES-256-CBC  100          271          10000        0.069735050201416
AES-256-CBC  100          270          100000       0.5698390007019
AES-256-CBC  100          272          250000       1.4465789794922

DECRYPT
CYPHER       LEN          LOOPS        Ellapsed
AES-128-CBC  100          100          0.00085186958312988
AES-128-CBC  100          1000         0.0087919235229492
AES-256-CBC  100          100          0.00084900856018066
AES-256-CBC  100          1000         0.0084569454193115
AES-256-CBC  100          10000        0.087380170822144
AES-256-CBC  100          100000       1.043438911438
AES-256-CBC  100          250000       2.3091888427734


Time: 5.05 seconds, Memory: 4.00MB
```
