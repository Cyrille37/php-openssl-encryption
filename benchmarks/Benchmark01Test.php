<?php

namespace Cyrille37\Benchmarks ;

use RuntimeException;
use PHPUnit\Framework\TestCase;
use Cyrille37\Crypto\OpenSslEncrypter ;
use Cyrille37\Crypto\DecryptException ;
use Cyrille37\Crypto\EncryptException ;

class Benchmark01Test extends TestCase
{
    const PAD=12;
    public function test01()
    {

        if( file_exists('/proc/cpuinfo'))
        {
            $cpuinfo = file_get_contents('/proc/cpuinfo');
            if( preg_match('/^model name\s:\s(.*)$/m',$cpuinfo,$matches) )
            {
                echo "\n", $matches[1],"\n";
            }    
        }

        echo "\nENCRYPT";
        echo "\n",str_pad('CYPHER',self::PAD,' '),' ',str_pad('CLEAR_LEN',self::PAD,' '),' ',str_pad('CRYPT_LEN',self::PAD,' '),' ',str_pad('LOOPS',self::PAD,' '),' ','Ellapsed', "\n";

        $this->encrypt(100,100,'AES-128-CBC');
        $this->encrypt(100,1000,'AES-128-CBC');
        $this->encrypt(100,100,'AES-256-CBC');
        $this->encrypt(100,1000,'AES-256-CBC');

        $this->encrypt(100,10000,'AES-256-CBC');
        $this->encrypt(100,100000,'AES-256-CBC');
        $this->encrypt(100,250000,'AES-256-CBC');

        echo "\nDECRYPT";
        echo "\n",str_pad('CYPHER',self::PAD,' '),' ',str_pad('LEN',self::PAD,' '),' ',str_pad('LOOPS',self::PAD,' '),' ','Ellapsed', "\n";

        $this->decrypt(100,100,'AES-128-CBC');
        $this->decrypt(100,1000,'AES-128-CBC');
        $this->decrypt(100,100,'AES-256-CBC');
        $this->decrypt(100,1000,'AES-256-CBC');

        $this->decrypt(100,10000,'AES-256-CBC');
        $this->decrypt(100,100000,'AES-256-CBC');
        $this->decrypt(100,250000,'AES-256-CBC');
    }

    protected function encrypt($len, $loops, $cypher)
    {
        switch($cypher)
        {
            case 'AES-128-CBC':
                $k = str_repeat('a', 16);
                break;
            case 'AES-256-CBC':
                $k = str_repeat('a', 32);
                break;
        }
        $e = new OpenSslEncrypter($k,$cypher);

        $p = random_bytes($len);

        $start = microtime(true);
        for( $i=0; $i<$loops; $i++ )
        {
            $encrypted = $e->encrypt($p);
        }
        $end = microtime(true);

        echo str_pad($cypher,self::PAD,' '),
            ' ',str_pad($len,self::PAD,' '),
            ' ',str_pad(strlen($encrypted),self::PAD,' '),
            ' ',str_pad($loops,self::PAD,' '),
            ' ', $end-$start, "\n";
        $this->assertNotEquals($p, $encrypted);
        $this->assertEquals($p, $e->decrypt($encrypted));
    }

    protected function decrypt($len, $loops, $cypher)
    {
        switch($cypher)
        {
            case 'AES-128-CBC':
                $k = str_repeat('a', 16);
                break;
            case 'AES-256-CBC':
                $k = str_repeat('a', 32);
                break;
        }
        $e = new OpenSslEncrypter($k,$cypher);

        $p = random_bytes($len);
        $encrypted = $e->encrypt($p);

        $start = microtime(true);
        for( $i=0; $i<$loops; $i++ )
        {
            $decrypted = $e->decrypt($encrypted);
        }
        $end = microtime(true);

        echo str_pad($cypher,self::PAD,' '),' ',str_pad($len,self::PAD,' '),' ',str_pad($loops,self::PAD,' '), ' ', $end-$start, "\n";
    }
}